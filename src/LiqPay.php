<?php

namespace code2magic\LiqPay;

use code2magic\LiqPay\Contract\CredentialInterface;
use code2magic\LiqPay\Contract\EndpointInterface;
use code2magic\LiqPay\Credential\Credential;
use code2magic\LiqPay\Endpoint\ApiEndpoint;
use code2magic\LiqPay\Endpoint\CustomEndpoint;
use code2magic\LiqPay\Helper\Currency;
use code2magic\LiqPay\Helper\Data;
use code2magic\LiqPay\Helper\Signature;

/**
 * Class LiqPay
 * @package code2magic\LiqPay
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class LiqPay
{
    private $_server_response_code = null;

    /**
     * @var CredentialInterface
     */
    private $_credential;

    /**
     * @var EndpointInterface
     */
    private $_endpoint;

    /**
     * LiqPay constructor.
     * @param $public_key
     * @param $private_key
     * @param null $api_url
     */
    public function __construct($public_key, $private_key, $api_url = null)
    {
        $this->_credential = new Credential($public_key, $private_key);
        if (null !== $api_url) {
            $this->setEndpoint(new CustomEndpoint($api_url));
        }
    }

    /**
     * @return EndpointInterface|ApiEndpoint
     */
    public function getEndpoint()
    {
        if (!$this->_endpoint) {
            $this->_endpoint = new ApiEndpoint();
        }
        return $this->_endpoint;
    }

    /**
     * @param EndpointInterface $endpoint
     * @return static
     */
    public function setEndpoint(EndpointInterface $endpoint)
    {
        $this->endpoint = $endpoint;
        return $this;
    }

    /**
     * @param $path
     * @param array $params
     * @param int $timeout
     * @return mixed
     */
    public function api($path, $params = array(), $timeout = 5)
    {
        if (!isset($params['version'])) {
            throw new \InvalidArgumentException('version is null');
        }
        $params['public_key'] = $this->_credential->getPublicKey();
        $data = Data::encode($params);
        $postfields = http_build_query(array(
            'data' => $data,
            'signature' => Signature::calculate($data, $this->_credential->getPrivateKey())
        ));
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->getEndpoint()->getUrl() . $path);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true); // Avoid MITM vulnerability http://phpsecurity.readthedocs.io/en/latest/Input-Validation.html#validation-of-input-sources
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);    // Check the existence of a common name and also verify that it matches the hostname provided
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);   // The number of seconds to wait while trying to connect
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);          // The maximum number of seconds to allow cURL functions to execute
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        $this->_server_response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return json_decode($server_output);
    }

    /**
     * @return null
     */
    public function get_response_code()
    {
        return $this->_server_response_code;
    }

    /**
     * @param $params
     * @return string
     */
    public function cnb_form($params)
    {
        $language = isset($params['language']) && $params['language'] == 'en' ? 'en' : 'ru';
        $form_data = $this->cnb_form_raw($params);
        return sprintf('
            <form method="POST" action="%s" accept-charset="utf-8">
                %s
                %s
                <input type="image" src="//static.liqpay.ua/buttons/p1%s.radius.png" name="btn_text" />
            </form>
            ',
            $form_data['url'],
            sprintf('<input type="hidden" name="%s" value="%s" />', 'data', $form_data['data']),
            sprintf('<input type="hidden" name="%s" value="%s" />', 'signature', $form_data['signature']),
            $language
        );
    }

    /**
     * @param $params
     * @return array
     */
    public function cnb_form_raw($params)
    {
        $params = $this->cnb_params($params);
        $data = Data::encode($params);
        return array(
            'url' => $this->getEndpoint()->getUrl() . '/3/checkout',
            'data' => $data,
            'signature' => Signature::calculate($data, $this->_credential->getPrivateKey())
        );
    }

    /**
     * cnb_signature
     *
     * @param array $params
     *
     * @return string
     * @deprecated
     */
    public function cnb_signature($params)
    {
        $params = $this->cnb_params($params);
        $data = Data::encode($params);
        return Signature::calculate($data, $this->_credential->getPrivateKey());
    }

    /**
     * @param $params
     * @return mixed
     */
    private function cnb_params($params)
    {
        $params['public_key'] = $this->_credential->getPublicKey();
        if (!isset($params['version'])) {
            throw new \InvalidArgumentException('version is null');
        }
        if (!isset($params['amount'])) {
            throw new \InvalidArgumentException('amount is null');
        }
        $params['currency'] = Currency::prepare($params['currency']);
        if (!isset($params['description'])) {
            throw new \InvalidArgumentException('description is null');
        }
        return $params;
    }

    /**
     * @param $params
     * @return string
     */
    private function encode_params($params)
    {
        return Data::encode($params);
    }

    /**
     * @param $params
     * @return mixed
     */
    public function decode_params($params)
    {
        return Data::decode($params);
    }

    /**
     * str_to_sign
     *
     * @param string $str
     *
     * @return string
     * @deprecated
     */
    public function str_to_sign($str)
    {
        return base64_encode(sha1($str, 1));
    }

    /**
     * @param $data
     * @param $signature
     * @return bool
     */
    public function validateData($data, $signature)
    {
        $data = is_string($data) ? $data : Data::encode($data);
        return Signature::calculate($data, $this->_credential->getPrivateKey()) === $signature;
    }
}
