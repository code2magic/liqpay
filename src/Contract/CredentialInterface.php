<?php
namespace code2magic\LiqPay\Contract;

interface CredentialInterface
{
    /**
     * @return string
     */
    public function getPublicKey();

    /**
     * @return string
     */
    public function getPrivateKey();
}