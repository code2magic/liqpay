<?php
namespace code2magic\LiqPay\Contract;

interface EndpointInterface
{
    /**
     * @return string
     */
    public function getUrl();

    /**
     * @return string
     */
    public function getMethod();
}