<?php
namespace code2magic\LiqPay\Endpoint;

use code2magic\LiqPay\Contract\EndpointInterface;

/**
 * Class CustomEndpoint
 * @package code2magic\LiqPay\Endpoint
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class CustomEndpoint implements EndpointInterface
{
    /**
     * @var string
     */
    private $_method;

    /**
     * @var string
     */
    private $_url;

    /**
     * CustomEndpoint constructor.
     * @param $url
     * @param $method
     */
    public function __construct($url, $method = 'POST')
    {
        $this->_url = $url;
        $this->_method = $method;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->_url;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->_method;
    }
}
