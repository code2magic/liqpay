<?php
namespace code2magic\LiqPay\Endpoint;

use code2magic\LiqPay\Contract\EndpointInterface;

/**
 * Class ApiEndpoint
 * @package code2magic\LiqPay\Endpoint
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class ApiEndpoint implements EndpointInterface
{
    /**
     * @return string
     */
    public function getUrl()
    {
        return 'https://www.liqpay.ua/api';
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return 'POST';
    }
}
