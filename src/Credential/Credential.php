<?php
namespace code2magic\LiqPay\Credential;

use code2magic\LiqPay\Contract\CredentialInterface;

/**
 * Class Credential
 * @package code2magic\LiqPay
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class Credential implements CredentialInterface
{
    /**
     * @var string
     */
    private $_public_key;

    /**
     * @var string
     */
    private $_private_key;

    /**
     * Credential constructor.
     * @param $public_key
     * @param $private_key
     */
    public function __construct($public_key, $private_key)
    {
        if (empty($public_key)) {
            throw new \InvalidArgumentException('public_key is empty');
        }
        if (empty($private_key)) {
            throw new \InvalidArgumentException('private_key is empty');
        }
        $this->_public_key = strval($public_key);
        $this->_private_key = strval($private_key);
    }

    /**
     * @return string
     */
    public function getPublicKey()
    {
        return $this->_public_key;
    }

    /**
     * @return string
     */
    public function getPrivateKey()
    {
        return $this->_private_key;
    }
}
