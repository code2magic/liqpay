<?php
namespace code2magic\LiqPay\Helper;

/**
 * Class Currency
 * @package code2magic\LiqPay\Helper
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class Currency
{
    const EUR = 'EUR';
    const USD = 'USD';
    const UAH = 'UAH';
    const RUB = 'RUB';
    const RUR = 'RUR';

    protected static $_supportedCurrencies = array(
        self::EUR,
        self::USD,
        self::UAH,
        self::RUB,
        self::RUR,
    );

    /**
     * @param $currency_code
     * @return string
     */
    public static function prepare($currency_code){
        if (!isset($currency_code)) {
            throw new \InvalidArgumentException('currency is null');
        }
        if (!in_array($currency_code, static::$_supportedCurrencies, true)) {
            throw new \InvalidArgumentException('currency is not supported');
        }
        if ($currency_code === self::RUR) {
            return self::RUB;
        }
        return $currency_code;
    }
}
