<?php
namespace code2magic\LiqPay\Helper;

/**
 * Class Signature
 * @package code2magic\LiqPay\Helper
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class Signature
{
    /**
     * @param string|array $data
     * @param string $private_key
     * @return string
     */
    public static function calculate($data, $private_key)
    {
        return base64_encode(sha1(
            $private_key . (is_string($data) ? $data : Data::encode($data)) . $private_key,
            1
        ));
    }
}
