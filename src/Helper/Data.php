<?php
namespace code2magic\LiqPay\Helper;

/**
 * Class Data
 * @package code2magic\LiqPay\Helper
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class Data
{
    /**
     * @param $params
     * @return string
     */
    public static function encode($params)
    {
        return base64_encode(json_encode($params));
    }

    /**
     * @param $params
     * @return mixed
     */
    public static function decode($params)
    {
        return json_decode(base64_decode($params), true);
    }
}
