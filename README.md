LiqPay
==========

## Installation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run
```bash
php composer.phar require code2magic/liqpay --prefer-dist
```
or add
```
"code2magic/liqpay": "*"
```

to the `require` section of your `composer.json` file.

## Usage
